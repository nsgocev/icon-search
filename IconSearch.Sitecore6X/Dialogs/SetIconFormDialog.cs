﻿using System.Collections.Generic;
using System.Linq;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.IO;
using Sitecore.Resources;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI;
using Edit = Sitecore.Web.UI.HtmlControls.Edit;

namespace IconSearch.Sitecore6X.Dialogs
{
    public class SetIconFormDialog : DialogForm
    {
        /// <summary>
        /// Gets or sets the applications list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The applications list.
        /// </value>
        protected Scrollbox ApplicationsList { get; set; }

        /// <summary>
        /// Gets or sets the business list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The business list.
        /// </value>
        protected Scrollbox BusinessList { get; set; }

        /// <summary>
        /// Gets or sets the controls list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The controls list.
        /// </value>
        protected Scrollbox ControlsList { get; set; }

        /// <summary>
        /// Gets or sets the core1 list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The core1 list.
        /// </value>
        protected Scrollbox Core1List { get; set; }

        /// <summary>
        /// Gets or sets the core2 list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The core2 list.
        /// </value>
        protected Scrollbox Core2List { get; set; }

        /// <summary>
        /// Gets or sets the core3 list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The core3 list.
        /// </value>
        protected Scrollbox Core3List { get; set; }

        /// <summary>
        /// Gets or sets the database list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The database list.
        /// </value>
        protected Scrollbox DatabaseList { get; set; }

        /// <summary>
        /// Gets or sets the flags list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The flags list.
        /// </value>
        protected Scrollbox FlagsList { get; set; }

        /// <summary>
        /// Gets or sets the icon file.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The icon file.
        /// </value>
        protected Edit IconFile { get; set; }

        /// <summary>
        /// Gets or sets the imaging list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The imaging list.
        /// </value>
        protected Scrollbox ImagingList { get; set; }

        /// <summary>
        /// Gets or sets the multimedia list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The multimedia list.
        /// </value>
        protected Scrollbox MultimediaList { get; set; }

        /// <summary>
        /// Gets or sets the network list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The network list.
        /// </value>
        protected Scrollbox NetworkList { get; set; }

        /// <summary>
        /// Gets or sets the other list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The other list.
        /// </value>
        protected Scrollbox OtherList { get; set; }

        /// <summary>
        /// Gets or sets the people list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The people list.
        /// </value>
        protected Scrollbox PeopleList { get; set; }

        /// <summary>
        /// Gets or sets the recent list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The recent list.
        /// </value>
        protected Scrollbox RecentList { get; set; }

        /// <summary>
        /// Gets or sets the software list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The software list.
        /// </value>
        protected Scrollbox SoftwareList { get; set; }

        /// <summary>
        /// Gets or sets the all list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The all list.
        /// </value>
        protected Scrollbox AllList { get; set; }

        /// <summary>
        /// Gets or sets the tab strip.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The tab strip.
        /// </value>
        protected VerticalTabstrip TabStrip { get; set; }

        /// <summary>
        /// Gets or sets the word processing list.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The word processing list.
        /// </value>
        protected Scrollbox WordProcessingList { get; set; }

        /// <summary>
        /// Raises the load event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        /// <remarks>
        /// This method notifies the server control that it should perform actions common to each HTTP
        ///             request for the page it is associated with, such as setting up a database query. At this
        ///             stage in the page lifecycle, server controls in the hierarchy are created and initialized,
        ///             view state is restored, and form controls reflect client-side data. Use the IsPostBack
        ///             property to determine whether the page is being loaded in response to a client postback,
        ///             or if it is being loaded and accessed for the first time.
        /// </remarks>
        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);
            TabStrip.OnChange += TabStrip_OnChange;
            if (Context.ClientPage.IsEvent)
                return;
            Item itemFromQueryString = UIUtil.GetItemFromQueryString(Database.GetDatabase(WebUtil.GetQueryString("sc_content", Context.ContentDatabase.Name)));
            Assert.IsNotNull(itemFromQueryString, typeof(Item));
            string queryString = WebUtil.GetQueryString("fld_id");
            if (string.IsNullOrEmpty(queryString))
                IconFile.Value = itemFromQueryString.Appearance.Icon;
            else
                IconFile.Value = itemFromQueryString.Fields[ID.Parse(queryString)].Value;
            RenderIcons();
            RenderRecentIcons();
        }

        /// <summary>
        /// Handles a click on the OK button.
        /// </summary>
        /// <param name="sender">The sender.</param><param name="args">The arguments.</param>
        /// <remarks>
        /// When the user clicks OK, the dialog is closed by calling
        ///             the <see cref="M:Sitecore.Web.UI.Sheer.ClientResponse.CloseWindow">CloseWindow</see> method.
        /// </remarks>
        protected override void OnOK(object sender, EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            SheerResponse.SetDialogValue(IconFile.Value);
            base.OnOK(sender, args);
        }

        /// <summary>
        /// Handles the OnChange event of the TabStrip control.
        /// </summary>
        /// <param name="sender">The source of the event.</param><param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void TabStrip_OnChange(object sender, EventArgs e)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(e, "e");
            SheerResponse.Eval("scUpdateControls();");
        }

        /// <summary>
        /// Draws the icons.
        /// </summary>
        /// <param name="prefix">The prefix.</param><param name="img">The img.</param><param name="area">The area.</param>
        private static void DrawIcons(string prefix, string img, string area)
        {
            string[] files = GetFiles(prefix);
            int num1 = files.Length;
            if (num1 == 0)
                num1 = 1;
            using (Bitmap bitmap1 = new Bitmap(960, (num1 / 24 + (num1 % 24 == 0 ? 0 : 1)) * 40, PixelFormat.Format32bppArgb))
            {
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(new StringWriter());
                htmlTextWriter.WriteLine("<map name=\"" + prefix + "\">");
                string relativeIconPath = prefix + "/32x32/";
                int num2 = 0;
                using (Graphics graphics = Graphics.FromImage(bitmap1))
                {
                    foreach (string path in files)
                    {
                        int num3 = num2 % 24;
                        int num4 = num2 / 24;
                        string themedImageSource = Images.GetThemedImageSource(relativeIconPath + path, ImageDimension.id32x32);
                        try
                        {
                            using (Bitmap bitmap2 = Settings.Icons.UseZippedIcons ? new Bitmap(ZippedIcon.GetStream(relativeIconPath + path, ZippedIcon.GetZipFile(relativeIconPath))) : new Bitmap(FileUtil.MapPath(themedImageSource)))
                                graphics.DrawImage(bitmap2, num3 * 40 + 4, num4 * 40 + 4, 32, 32);
                            string str1 = string.Format("{0},{1},{2},{3}", (object)(num3 * 40 + 4), (object)(num4 * 40 + 4), (object)(num3 * 40 + 36), (object)(num4 * 40 + 36));
                            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);

                            if (fileNameWithoutExtension != null)
                            {
                                string str2 = StringUtil.Capitalize(fileNameWithoutExtension.Replace("_", " "));
                                htmlTextWriter.WriteLine("<area shape=\"rect\" coords=\"{0}\" href=\"#\" alt=\"{1}\" sc_path=\"{2}\"/>", str1, str2, relativeIconPath + path);
                            }
                            ++num2;
                        }
                        catch (Exception ex)
                        {
                            Log.Warn("Unable to open icon " + themedImageSource, ex, typeof(SetIconFormDialog));
                        }
                    }
                }
                htmlTextWriter.WriteLine("</map>");
                FileUtil.WriteToFile(area, htmlTextWriter.InnerWriter.ToString());
                bitmap1.Save(img, ImageFormat.Png);
            }
        }

        /// <summary>
        ///  Draws All Icons
        /// </summary>
        /// <param name="img"></param>
        /// <param name="area"></param>
        private static void DrawAllIcons(string img, string area)
        {
            string[] files = GetFiles("All");
            int num1 = files.Length;
            if (num1 == 0)
                num1 = 1;


            using (Bitmap bitmap1 = new Bitmap(960, (num1 / 24 + (num1 % 24 == 0 ? 0 : 1)) * 40, PixelFormat.Format32bppArgb))
            {
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(new StringWriter());
                htmlTextWriter.WriteLine("<map name=\"" + "all" + "\">");

                int num2 = 0;
                using (Graphics graphics = Graphics.FromImage(bitmap1))
                {
                    foreach (string path in files)
                    {

                        int num3 = num2 % 24;
                        int num4 = num2 / 24;
                        string themedImageSource = Images.GetThemedImageSource(path, ImageDimension.id32x32);
                        try
                        {
                            using (Bitmap bitmap2 = Settings.Icons.UseZippedIcons ? new Bitmap(ZippedIcon.GetStream(path, ZippedIcon.GetZipFile(string.Format("{0}/32x32/", path.Split('/')[0])))) : new Bitmap(FileUtil.MapPath(themedImageSource)))
                                graphics.DrawImage(bitmap2, num3 * 40 + 4, num4 * 40 + 4, 32, 32);
                            string str1 = string.Format("{0},{1},{2},{3}", (object)(num3 * 40 + 4), (object)(num4 * 40 + 4), (object)(num3 * 40 + 36), (object)(num4 * 40 + 36));
                            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);

                            if (fileNameWithoutExtension != null)
                            {
                                string str2 = StringUtil.Capitalize(fileNameWithoutExtension.Replace("_", " "));
                                htmlTextWriter.WriteLine("<area shape=\"rect\" coords=\"{0}\" href=\"#\" alt=\"{1}\" sc_path=\"{2}\"/>", str1, str2, path);
                            }
                            ++num2;
                        }
                        catch (Exception ex)
                        {
                            Log.Warn("Unable to open icon " + themedImageSource, ex, typeof(SetIconFormDialog));
                        }
                    }
                }
                htmlTextWriter.WriteLine("</map>");
                FileUtil.WriteToFile(area, htmlTextWriter.InnerWriter.ToString());
                bitmap1.Save(img, ImageFormat.Png);
            }
        }

        /// <summary>
        /// Gets the filename.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// Returns the filename.
        /// </returns>
        private static string GetFilename(string prefix)
        {
            return FileUtil.MapPath(FileUtil.MakePath(TempFolder.Folder, "icons_" + prefix + ".png"));
        }

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// The files.
        /// </returns>
        private static string[] GetFiles(string prefix)
        {
            Assert.ArgumentNotNullOrEmpty(prefix, "prefix");

            if (!Settings.Icons.UseZippedIcons)
            {
                return prefix == "All" ? GetAllFolderFiles() : GetFolderFiles(prefix);
            }

            return prefix == "All" ? GetAllZippedFiles() : GetZippedFiles(prefix);
        }

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// Returns the files.
        /// </returns>
        private static string[] GetFolderFiles(string prefix)
        {
            string[] files = Directory.GetFiles(FileUtil.MapPath("/sitecore/shell/themes/standard/" + prefix + "/32x32"));
            for (int index = 0; index < files.Length; ++index)
                files[index] = Path.GetFileName(files[index]);
            return files;
        }

        private static string[] GetAllFolderFiles()
        {
            List<string> allFiles = new List<string>();

            allFiles.AddRange(GetFolderFiles("Applications").Select(x => string.Format("Applications/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Business").Select(x => string.Format("Business/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Control").Select(x => string.Format("Control/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Core").Select(x => string.Format("Core/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Core2").Select(x => string.Format("Core2/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Core3").Select(x => string.Format("Core3/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Database").Select(x => string.Format("Database/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Flags").Select(x => string.Format("Flags/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Imaging").Select(x => string.Format("Imaging/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Multimedia").Select(x => string.Format("Multimedia/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Network").Select(x => string.Format("Network/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Other").Select(x => string.Format("Other/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("People").Select(x => string.Format("People/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("Software").Select(x => string.Format("Software/32x32/{0}", x)));
            allFiles.AddRange(GetFolderFiles("WordProcessing").Select(x => string.Format("WordProcessing/32x32/{0}", x)));

            return allFiles.ToArray();
        }

        /// <summary>
        /// Gets the zipped filed.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// Returns the zipped filed.
        /// </returns>
        private static string[] GetZippedFiles(string prefix)
        {
            return ZippedIcon.GetFiles(prefix, "/sitecore/shell/themes/standard/" + prefix + ".zip");
        }

        private static string[] GetAllZippedFiles()
        {
            List<string> allPaths = new List<string>();

            allPaths.AddRange(GetZippedFiles("Applications").Select(x => string.Format("Applications/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Business").Select(x => string.Format("Business/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Control").Select(x => string.Format("Control/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Core").Select(x => string.Format("Core/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Core2").Select(x => string.Format("Core2/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Core3").Select(x => string.Format("Core3/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Database").Select(x => string.Format("Database/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Flags").Select(x => string.Format("Flags/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Imaging").Select(x => string.Format("Imaging/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Multimedia").Select(x => string.Format("Multimedia/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Network").Select(x => string.Format("Network/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Other").Select(x => string.Format("Other/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("People").Select(x => string.Format("People/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("Software").Select(x => string.Format("Software/32x32/{0}", x)));
            allPaths.AddRange(GetZippedFiles("WordProcessing").Select(x => string.Format("WordProcessing/32x32/{0}", x)));

            return allPaths.ToArray();
        }

        /// <summary>
        /// Renders the icons.
        /// </summary>
        /// <param name="scrollbox">The scrollbox.</param><param name="prefix">The prefix.</param>
        private static void RenderIcons(Scrollbox scrollbox, string prefix)
        {
            Assert.ArgumentNotNull(scrollbox, "scrollbox");
            Assert.ArgumentNotNullOrEmpty(prefix, "prefix");
            string filename = GetFilename(prefix);
            string str = Path.ChangeExtension(filename, ".html");
            if (!File.Exists(filename) || !File.Exists(str))
            {
                if (prefix == "All")
                {
                    DrawAllIcons(filename, str);
                }
                else
                {
                    DrawIcons(prefix, filename, str);
                }
            }

            HtmlTextWriter output = new HtmlTextWriter(new StringWriter());
            output.Write(FileUtil.ReadFromFile(str));
            WriteImageTag(filename, prefix, output);
            scrollbox.InnerHtml = output.InnerWriter.ToString();
        }

        /// <summary>
        /// Renders the icons.
        /// </summary>
        private void RenderIcons()
        {
            RenderIcons(ApplicationsList, "Applications");
            RenderIcons(BusinessList, "Business");
            RenderIcons(ControlsList, "Control");
            RenderIcons(Core1List, "Core");
            RenderIcons(Core2List, "Core2");
            RenderIcons(Core3List, "Core3");
            RenderIcons(DatabaseList, "Database");
            RenderIcons(FlagsList, "Flags");
            RenderIcons(ImagingList, "Imaging");
            RenderIcons(MultimediaList, "Multimedia");
            RenderIcons(NetworkList, "Network");
            RenderIcons(OtherList, "Other");
            RenderIcons(PeopleList, "People");
            RenderIcons(SoftwareList, "Software");
            RenderIcons(WordProcessingList, "WordProcessing");
            RenderIcons(AllList, "All");
        }

        /// <summary>
        /// Renders the recent icons.
        /// </summary>
        private void RenderRecentIcons()
        {
            int num = 0;
            ListString listString = new ListString(Registry.GetString("/Current_User/RecentIcons"));
            HtmlTextWriter htmlTextWriter = new HtmlTextWriter(new StringWriter());
            ImageBuilder imageBuilder = new ImageBuilder
            {
                Width = 32,
                Height = 32
            };
            foreach (string str in listString)
            {
                imageBuilder.Src = Images.GetThemedImageSource(str, ImageDimension.id32x32);

                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(str);

                if (fileNameWithoutExtension != null)
                {
                    imageBuilder.Alt = StringUtil.Capitalize(fileNameWithoutExtension.Replace("_", " "));
                }

                imageBuilder.Class = "scRecentIcon";
                htmlTextWriter.Write(imageBuilder.ToString());
                ++num;
            }
            if (num == 0)
            {
                htmlTextWriter.Write("<div align=\"center\" style=\"padding:32px 0px 0px 0px\"><i>");
                htmlTextWriter.Write(Translate.Text("There are no items in this list."));
                htmlTextWriter.Write("</i></div>");
            }
            RecentList.InnerHtml = htmlTextWriter.InnerWriter.ToString();
        }

        /// <summary>
        /// Writes the image tag.
        /// 
        /// </summary>
        /// <param name="img">The img.</param><param name="prefix">The prefix.</param><param name="output">The output.</param>
        private static void WriteImageTag(string img, string prefix, HtmlTextWriter output)
        {
            Assert.ArgumentNotNull(img, "img");
            Assert.ArgumentNotNull(prefix, "prefix");
            Assert.ArgumentNotNull(output, "output");
            ImageBuilder imageBuilder;
            using (Bitmap bitmap = new Bitmap(img))
                imageBuilder = new ImageBuilder
                {
                    Src = FileUtil.UnmapPath(img),
                    Width = bitmap.Width,
                    Height = bitmap.Height,
                    Usemap = "#" + prefix
                };
            output.WriteLine(imageBuilder.ToString());
        }
    }
}